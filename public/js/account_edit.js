$(document).ready(function () {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var dataChanged = false;
    $('#account #tab3').find('input').keydown(function() {
        dataChanged = true;
    });

    $( "#account #tab3" ).focusout(function() {
        if(dataChanged) {
            var username = $(this).find('#name').val();
            var phone = $(this).find('#phone').val();
            $.ajax({
                url: 'user/update/' + userID,
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    data: {
                        type: 'account_info',
                        username: username,
                        phone: phone
                    }
                },
                dataType: 'JSON',
                success: function (data) {
                    console.log(data['response']);
                    notify('Powodzenie.', 'Dane zostały poprawnie zapisane.', 'success', 1000);
                    dataChanged = false;
                },
                error: function(data){
                    notify('Niepowodzenie: ', 'Nie udało się zapisać danych. Spróbuj ponownie.', 'danger');
                }
            });
        }
    });
});