var map, infoWindow, service, pos;
var markers = [];
var actual_count_id = 0;
var sv;
var setStreetView;
var panorama;
var directions = [];
var directionsService;
var lastDest;
var watchID;

function initMap() {
    pos = new google.maps.LatLng(52.230591, 21.014752);
    sv = new google.maps.StreetViewService();
    //WYSZUKIWANIE TRASY

    directionsService = new google.maps.DirectionsService;

    panorama = new google.maps.StreetViewPanorama(document.querySelector("#panorama .modal-body"));

    map = new google.maps.Map(document.getElementById('map'), {
        center: pos,
        zoom: 17,
        disableDefaultUI: true,
        styles: myStyle
    });

    setStreetView = function (newPlace) {
        sv.getPanoramaByLocation(newPlace, 200, processSVData);
    };

    service = new google.maps.places.PlacesService(map);
    infoWindow = new google.maps.InfoWindow;


    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            map.setCenter(pos);

        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });

    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function renderDirections(result) {
    var directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(map);
    directionsDisplay.setDirections(result);
    directions.push(directionsDisplay);
}

function requestDirections(start, end) {
    var selectedMode = "WALKING";
    lastDest = end;

    directionsService.route({
        origin: start,
        destination: end,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode[selectedMode]
    }, function (result) {
        renderDirections(result);
        $('#place_details').modal('hide');
    });
}

function processSVData(data, status) {
    if (status === 'OK') {
        panorama.setPano(data.location.pano);
        panorama.setPov({
            heading: 270,
            pitch: 0
        });

        $("#panorama").on("shown.bs.modal", function () {
            google.maps.event.trigger(panorama, 'resize');
        });

    } else {
        console.error('Street View data not found for this location.');
    }
}

function processResults(results, status, pagination) {
    if (status !== google.maps.places.PlacesServiceStatus.OK) {
        notify("", "Niestety nie znaleziono nic w pobliżu. Spróbuj wybrać inną kateogorię", "info", 750);
        return;
    } else {
        $('#places').find('li:not(.sidebar-brand)').remove();
        $('#moreCategories').modal('hide');
        clearDirections();

        lastDest = null;

        if (!$(".menu-right").hasClass("toggled"))
            $(".menu-right").addClass("toggled");

        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }

        createMarkers(results);
        $("#place_details").swipe({
            //Generic swipe handler for all directions
            swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                if (direction == 'left') {
                    if (actual_count_id + 1 < results.length) {
                        showPlaceDetails(results[actual_count_id + 1].place_id);
                        actual_count_id += 1;
                    }
                } else if (direction == 'right') {
                    if (actual_count_id - 1 >= 0) {
                        showPlaceDetails(results[actual_count_id - 1].place_id);
                        actual_count_id -= 1;
                    }

                }
            },
            allowPageScroll: "vertical"
        });

        if (pagination.hasNextPage) {
            /*moreButton.addEventListener('click', function() {
             moreButton.disabled = true;
             pagination.nextPage();
             });*/
        }
    }
}

function createMarkers(places) {
    var bounds = new google.maps.LatLngBounds();
    var placesList = document.getElementById('places');

    for (var i = 0; i < places.length; i++) {

        let place = places[i];
        place.count_id = i;

        var image = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
        };

        var marker = new google.maps.Marker({
            map: map,
            icon: image,
            title: place.name,
            position: place.geometry.location
        });

        marker.addListener('click', function () {
            showPlaceDetails_Action(place.place_id, place.count_id);
        });

        markers.push(marker);

        var newListItem = document.createElement("li");
        newListItem.className = "list-group-item";
        newListItem.innerHTML = place.name;
        newListItem.addEventListener('click', function () {
            showPlaceDetails_Action(place.place_id, place.count_id);
        });

        placesList.append(newListItem);

        bounds.extend(place.geometry.location);

    }
    map.fitBounds(bounds);
}
function showPlaceDetails_Action(place_id, tick) {
    showPlaceDetails(place_id);
    actual_count_id = tick;
}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Błąd: The Geolocation service failed.' :
        'Błąd: Twoja przeglądarka nie wspiera gelokalizacji. Brak HTTP(S)');
    infoWindow.open(map);
}

function clearDirections() {
    directions.forEach(function (direction) {
        direction.setMap(null);
        direction = null;
    });
    directions = [];
}

