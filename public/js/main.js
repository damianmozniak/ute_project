$('document').ready(function () {
    window.notify = function (title, msg, type, delay) {
        $.notify({
            icon: 'glyphicon glyphicon-warning-sign',
            title: title,
            message: msg
        }, {
            type: type,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            z_index: 9999,
            delay: delay ? delay : 5000
        });
    };

    var timeout_id = 0;
    var hold_time = 1000;
    var addingCat = false;
    var clickedButton = null;
    var place_details = $('#place_details');

    //WYBIERANIE KATEOGRII -- WYSZUKIWANIE
    var moreCategoriesModal = $('#moreCategories');
    $('#categories, #moreCategories').on('mousedown touchstart', '.btn', function (event) {
        clickedButton = $(this);
        timeout_id = setTimeout(addFavCategory, hold_time);

    }).on('mouseup touchend', '.btn', function (event) {
        clearTimeout(timeout_id);

        if (!addingCat && event.pageY) {
            if ($(this).attr('data-type')) {
                service.nearbySearch({
                    location: pos,
                    radius: moreCategoriesModal.find(".rangeslider-container input").val(),
                    type: [$(this).attr('data-type')]
                }, processResults);
            }
        } else {
            addingCat = false;
        }
    });

    function addFavCategory() {
        addingCat = true;
        var cat_name = clickedButton.attr('data-type');
        var fav_cat = JSON.parse(localStorage.getItem("favourite_categories"));

        if (clickedButton.hasClass('btn-info')) {
            clickedButton.removeClass('btn-info');
            clickedButton.addClass('btn-success');

        } else if (clickedButton.hasClass('btn-success')) {
            clickedButton.removeClass('btn-success');
            clickedButton.addClass('btn-info');
        }


        if (fav_cat.indexOf(cat_name) > -1) {
            var index = fav_cat.indexOf(cat_name);
            fav_cat.splice(index, 1);
            notify('', 'Usunięto kategorię z ulubionych', 'info', 1000);
        } else {
            fav_cat.push(cat_name);
            notify('', 'Dodano kategorię do ulubionych', 'success', 1000);
        }

        localStorage.setItem("favourite_categories", JSON.stringify(fav_cat));
    }


    window.toggleMenu = function (menu) {
        $(menu).toggleClass("toggled");
    };

    //GORNE PRZYCISKI
    $(".btn#menu-left").click(function () {
        toggleMenu(".menu-left");
    });
    $(".btn#menu-right").click(function () {
        toggleMenu(".menu-right");
    });

    //ACCOUNT EDIT
    $(".btn-pref .btn").click(function () {
        $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
        $(this).removeClass("btn-default").addClass("btn-primary");
    });

    //Blokowanie przekierowania na /#
    $("body").on('click', 'a[href="#"]', function (e) {
        e.preventDefault();
    });

    //USTAWIANIE KLAS PRZYCISKOW KATEGORII
    if (!localStorage.getItem("favourite_categories")) {
        localStorage.setItem("favourite_categories", "[]");
        moreCategoriesModal.find('button').addClass('btn-info');
    } else {
        var fav_cat = JSON.parse(localStorage.getItem("favourite_categories"));
        moreCategoriesModal.find('button').each(function () {
            var button = $(this);

            if (fav_cat.includes(button.attr('data-type'))) { //KATEGORIA W ULUBIONYCH
                button.addClass('btn-success');
                button.remove();
                button.insertAfter('#moreCategories .rangeslider-container');
            } else {
                button.addClass('btn-info');
            }
        });
    }

    //SLIDER WYBORU PROMIENIA WYSZUKIWANIA
    $('input[type="range"]').rangeslider({
        polyfill: false,
        onSlide: function (position, value) {
            moreCategoriesModal.find("div.rangeslider-info > .range").text(value);
            localStorage.setItem('finding_range', value);
        }
    });



    //DOUBLE CLICK PLACE DETAILS
    var tapped=false;

    place_details.on('touchstart', '.modal-body',function(e){
        if(!tapped){ //if tap is not set, set up single tap
            tapped=setTimeout(function(){
                tapped=null;
                //insert things you want to do when single tapped
            },200);   //wait 300ms then run single click code
        } else {    //tapped within 300ms of last tap. double tap
            clearTimeout(tapped); //stop single tap callback
            tapped=null;

            place_details.modal('hide');
        }
        //e.preventDefault()
    });

    //SWIPE DLA BOCZNYCH MENU
    $("#wrapper.menu-left").swipe( {
        //Generic swipe handler for all directions
        swipeLeft:function(event, direction) {
            $(this).removeClass('toggled');
        },
        allowPageScroll: "vertical"
    });
    $("#wrapper.menu-right").swipe( {
        //Generic swipe handler for all directions
        swipeRight:function(event, direction) {
            $(this).removeClass('toggled');
        },
        allowPageScroll: "vertical"
    });

    //StreetView
    place_details.on('click', '.street_view', function(){
        var lat = parseFloat(place_details.find('input[name="lat"]').val());
        var lng = parseFloat(place_details.find('input[name="lng"]').val());
        var newPlace = new google.maps.LatLng(lat,lng);

        setStreetView(newPlace);
        $('#panorama').modal('show');
    });

    //FOLLOW ACTIVATION
    $('#follow').click(function(){
        if(!$(this).hasClass('btn-info')) {
            watchID = navigator.geolocation.watchPosition(function(position) {
                map.setCenter({ lat: position.coords.latitude, lng: position.coords.longitude});
                if(lastDest) {
                    clearDirections();
                    requestDirections(pos, lastDest);
                }

            });
        }else{
            navigator.geolocation.clearWatch(watchID);
        }
        $('#follow').toggleClass('btn-info');
    });

    //POKAZ DROGE DO CELU SINGLE
    place_details.on('click', '.show-path', function(){
        var lat = parseFloat(place_details.find('input[name="lat"]').val());
        var lng = parseFloat(place_details.find('input[name="lng"]').val());
        var newPlace = new google.maps.LatLng(lat,lng);

        clearDirections();
        requestDirections(pos, newPlace);
    });

    //Invite
    place_details.on('click', '.invite', function(){
        var lat = parseFloat(place_details.find('input[name="lat"]').val());
        var lng = parseFloat(place_details.find('input[name="lng"]').val());
        var new_place_pos = new google.maps.LatLng(lat,lng);
        var new_place_id = place_details.find('input[name="id"]').val();

        inviteNearPeoples(new_place_pos, new_place_id);
    });


    //DEFAULT FINDING RANGE
    if(localStorage.getItem('finding_range')) {
        $('#moreCategories').find('.rangeslider--horizontal').val(parseInt(localStorage.getItem('finding_range')));
        $('input[type="range"]').rangeslider('update', true);
    }

});