var place_details;
var locations_refresh_timeout;
/**
 * Renders modal "Place Details" by place id
 * @param {Number} id
 * @param {Boolean} hide
 */
function showPlaceDetails(id, hide) {
    clearTimeout(locations_refresh_timeout);
    service.getDetails({
        placeId: id
    }, function (place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            var data = place;

            localStorage.setItem("last_show", id);


            //USTAWIANIE LINKOW ZDJEC
            data.photoUrls = [];

            if (data.photos !== undefined) {
                var photos_count = 5;

                if (data.photos.length < 5)
                    photos_count = data.photos.length;

                for (let i = 0; i < photos_count; i++) {
                    data.photoUrls.push({
                        'index': i + 1,
                        'url': data.photos[i].getUrl({
                            maxHeight: 350
                        })
                    });
                }
            }
            data.lat = data.geometry.location.lat();
            data.lng = data.geometry.location.lng();
            data.place_id = place.place_id;

            //SPRAWDZ CZY OTWARTE I USTAW TEXT
            if (data.opening_hours) {
                if (data.opening_hours.open_now) {
                    data.status = "OTWARTE";
                    data.status_color = "green";
                } else {
                    data.status = "ZAMKNIĘTE";
                    data.status_color = "palevioletred";
                }

            } else {
                data.status = "Brak Danych";
                data.status.color = "orange";
            }


            if (data.rating)
                rating = Math.floor(data.rating);
            else {
                data.rating = "brak";
            }


            var template = $("#place_details_template").html();
            var place_details_template = Mustache.render(template, data);
            place_details = $('#place_details');
            var rating;
            place_details.html(place_details_template);

            place_details.find('.preview-pic > div:eq(0)').addClass('active');
            place_details.find('.preview-thumbnail > li:eq(0)').addClass('active');
            place_details.find('.product-description > p:eq(0)').addClass('active');

            for (let i = 0; i < 5; i++) {
                if (i < rating) {
                    place_details.find('.rating .stars > span:eq( ' + i + ' )').addClass('checked');
                }
            }
            if (!hide)
                place_details.modal('show');

            //JEZELI UCZESTNICZY W WYDARZENIU TO POKAZ PRZYCISK DO OPUSZCZENIA
            if(window.userID) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: 'event/user_in/' + data.place_id,
                    type: 'POST',
                    data: {
                        _token: CSRF_TOKEN
                    },
                    dataType: 'JSON',
                    success: function (user_in_event) {
                        if(user_in_event) {
                            place_details.find('.quit').removeClass('hidden');
                        }
                    }
                });
                place_details.find('.quit').click(function(){
                    $.ajax({
                        url: 'event/quit/' + data.place_id,
                        type: 'POST',
                        data: {
                            _token: CSRF_TOKEN
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            notify('', data['response']['msg'], data['response']['type'], 1000);
                            place_details.find('.quit').addClass('hidden');
                        }
                    });
                    clearTimeout(locations_refresh_timeout);
                });
            }
        } else {
            notify('Nie udało się pobrać danych. Spróbuj ponownie', '', 'warning', '1000');
        }
    });
}


function inviteNearPeoples(new_place_pos, new_place_id) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: 'event/' + new_place_id,
        type: 'POST',
        data: {
            _token: CSRF_TOKEN
        },
        dataType: 'JSON',
        success: function (data) {
            var url = window.location.href;
            var url_arr = url.split("/");
            let host = url_arr[0] + "//" + url_arr[2];

            let users_to_invite = data['response']['users'];
            let orange_api_key = data['response']['orange_api_key'];
            let from = data['response']['user_phone'];
            let msg = "W%20pobli%C5%BCu%20jest%20wydarzenie.%0AWejd%C5%BC%20w%20link%0A"+host+"/place/"+new_place_id;
            let distance;

            if(users_to_invite)
                users_to_invite.forEach(function (phone) {
                    $.ajax({
                        url: 'https://apitest.orange.pl/Localization/v1/GeoLocation?msisdn=48'+phone+'&apikey='+orange_api_key,
                        type: 'GET',
                        data: {
                            _token: CSRF_TOKEN
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            if(data['result'] == 'COMPLETED') {
                                var acceptor_position = new google.maps.LatLng( parseFloat(data['latitude']), parseFloat(data['longitude']) );
                                distance = google.maps.geometry.spherical.computeDistanceBetween(new_place_pos, acceptor_position);

                                if(distance < 20000) {
                                    $.ajax({
                                        url: 'https://apitest.orange.pl/Messaging/v1/SMSOnnet?from=48'+from+'&to=48'+phone+'&msg='+msg+'&apikey='+orange_api_key,
                                        type: 'GET',
                                        data: {
                                            _token: CSRF_TOKEN
                                        },
                                        dataType: 'JSON',
                                        success: function (data) {

                                        }
                                    });
                                }
                            }
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                });

            notify('', data['response']['msg'], data['response']['type'], 1000);

            if(!data['response']['fail'])
                place_details.find('.quit').removeClass('hidden');

            $('#place_details').modal('hide');

            function refreshLocations() {
                locations_refresh_timeout = setTimeout(function(){
                    clearDirections();
                    navigator.geolocation.getCurrentPosition(function (position) {
                        pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                    });

                    $.ajax({
                        url: 'event/' + new_place_id + '/lat/' + pos.lat + '/lng/' + pos.lng,
                        type: 'POST',
                        data: {
                            _token: CSRF_TOKEN
                        },
                        dataType: 'JSON'
                    });

                    $.ajax({
                        url: 'event/listeners/' + new_place_id,
                        type: 'GET',
                        data: {
                            _token: CSRF_TOKEN
                        },
                        dataType: 'JSON',
                        success: function (data) {
                            var listeners = data['response']['listeners'];
                            listeners.forEach(function(listener){
                                var acceptor_pos = new google.maps.LatLng(listener.lat,listener.lng);
                                requestDirections(acceptor_pos, new_place_pos);
                            });

                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                    refreshLocations();
                },15000);
            }
            refreshLocations();

            clearDirections();
            requestDirections(pos, new_place_pos);
        },
        error: function (data) {
            notify('Niepowodzenie: ', 'Zdarzenie nie zostało zrealizowane.', 'danger', 1500);
        }
    });
}
