<div id="categories" class="btn-group">
    <button type="button" class="btn btn-default btn-lg" aria-label="Left Align" data-type="store" title="Sklep">
        <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
    </button>
    <button type="button" class="btn btn-default btn-lg" aria-label="Left Align" data-type="bar" title="Bar">
        <span class="glyphicon glyphicon-glass" aria-hidden="true"></span>
    </button>
    <button type="button" class="btn btn-info btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#moreCategories" title="Więcej">
        <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
    </button>
    <button type="button" class="btn btn-success btn-lg" aria-label="Left Align" data-toggle="modal" data-target="#place_details" title="Ostatnio oglądane">
        <span class="fa fa-handshake-o" aria-hidden="true"></span>
    </button>
</div>