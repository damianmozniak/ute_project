<div id="wrapper" class="menu-left">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="#" onclick="toggleMenu('.menu-left')">UTE Project Title</a>
            </li>
            @if (Auth::check())
                <li class="profile-header-container text-center" data-toggle="modal" data-target="#account">
                    <div class="profile-header-img">
                        <img class="img-circle" src="{{ url('/images/default_avatar.jpg') }}" />
                        <!-- badge -->
                        <div class="rank-label-container">
                            <span class="label label-default rank-label">{{ $user->name }}</span>
                        </div>
                    </div>
                </li>
            @endif
            <li>
                <a href="{{ route('home') }}">Strona Główna</a>
            </li>
            @if (!Auth::check())
                <li>
                    <a href="{{ URL::route('user.login') }}">Logowanie</a>
                </li>
                <li>
                    <a href="{{ URL::route('user.signup') }}">Rejestracja</a>
                </li>
            @else
                <li>
                    <a href="{{ URL::route('user.logout') }}">Wyloguj</a>
                </li>
            @endif
        </ul>
    </div>
</div>