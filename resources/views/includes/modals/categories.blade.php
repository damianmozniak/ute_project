<div class="modal fade" id="moreCategories" tabindex="-1" role="dialog" aria-labelledby="moreCategories">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="rangeslider-container">
                    <input class="rangeslider--horizontal" type="range" min="100" max="2000" step="100" value="400" data-orientation="horizontal">
                    <div class="text-center rangeslider-info">
                        <span>Szukaj w pobliżu</span>
                        <span class="range">400</span>
                        <span>Metrów</span>
                    </div>
                </div>

                <button type="button" class="btn btn-md btn-block" data-type="accounting">Rachunkowość</button>
                <button type="button" class="btn btn-md btn-block" data-type="airport">Lotniska</button>
                <button type="button" class="btn btn-md btn-block" data-type="amusement_park">Park rozrywki</button>
                <button type="button" class="btn btn-md btn-block" data-type="aquarium">Akwarium</button>
                <button type="button" class="btn btn-md btn-block" data-type="art_gallery">Galeria Sztuki</button>
                <button type="button" class="btn btn-md btn-block" data-type="atm">Bankomat</button>
                <button type="button" class="btn btn-md btn-block" data-type="bakery">Piekarnia</button>
                <button type="button" class="btn btn-md btn-block" data-type="bank">Bank</button>
                <button type="button" class="btn btn-md btn-block" data-type="hospital">Szpital</button>
                <button type="button" class="btn btn-md btn-block" data-type="insurance_agency">Ubezpieczenia</button>
                <button type="button" class="btn btn-md btn-block" data-type="jewelry_store">Sklep Jubilerski</button>
                <button type="button" class="btn btn-md btn-block" data-type="laundry">Pralnia</button>
                <button type="button" class="btn btn-md btn-block" data-type="lawyer">Prawnik</button>
                <button type="button" class="btn btn-md btn-block" data-type="library">Biblioteka</button>
                <button type="button" class="btn btn-md btn-block" data-type="liquor_store">Sklep Alkoholowy</button>
                <button type="button" class="btn btn-md btn-block" data-type="local_government_office">Urząd</button>
                <button type="button" class="btn btn-md btn-block" data-type="locksmith">Ślusarz</button>
                <button type="button" class="btn btn-md btn-block" data-type="beauty_salon">Salon Piękności</button>
                <button type="button" class="btn btn-md btn-block" data-type="lodging">Kwatera/Mieszkanie</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
            </div>
        </div>
    </div>
</div>