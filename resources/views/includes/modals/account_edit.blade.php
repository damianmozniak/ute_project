<div class="modal fade" id="account" tabindex="-1" role="dialog" aria-labelledby="account">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <div class="card hovercard">
                            <div class="card-background">
                                <img class="card-bkimg" alt="" src="{{ url('/images/default_avatar.jpg') }}">
                            </div>
                            <div class="useravatar">
                                <img alt="" src="{{ url('/images/default_avatar.jpg') }}">
                            </div>
                            <div class="card-info">
                                <span class="card-title">{{ $user->name }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
                            {{--<div class="btn-group" role="group">
                                <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab">
                                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                    <div class="hidden-xs">Znajomi</div>
                                </button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="button" id="favorites" class="btn btn-default" href="#tab2"
                                        data-toggle="tab"><span class="glyphicon glyphicon-heart"
                                                                aria-hidden="true"></span>
                                    <div class="hidden-xs">Ulubione</div>
                                </button>
                            </div>--}}
                            <div class="btn-group" role="group">
                                <button type="button" id="following" class="btn btn-primary" href="#tab3"
                                        data-toggle="tab"><span class="glyphicon glyphicon-align-justify"
                                                                aria-hidden="true"></span>
                                    <div class="hidden-xs">Ustawienia</div>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="tab-content">
                            {{--<div class="tab-pane fade in active" id="tab1">
                                <h3>Znajomi użytkownicy, grupy itd</h3>
                            </div>
                            <div class="tab-pane fade in" id="tab2">
                                <h3>Ulubione kategorie</h3>
                            </div>--}}
                            <div class="tab-pane fade in active" id="tab3">
                                <div class="form-group">
                                    <label for="name">Nazwa użytkownika</label>
                                    <input class="form-control" type="text" value="{{ $user->name }}" id="name" placeholder="Albert Einstein">
                                </div>
                                <div class="form-group">
                                    <label for="phone">Numer telefonu</label>
                                    <input class="form-control" type="tel" value="{{ $user->phone }}" id="phone" maxlength="9" placeholder="515515515 Numer telefonu">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>