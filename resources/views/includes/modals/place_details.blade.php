<div class="modal fade" id="place_details" tabindex="-1" role="dialog" aria-labelledby="place_details"></div>
<script id="place_details_template" type="text/template">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <input name="lat" type="hidden" value="@{{ lat }}">
                <input name="lng" type="hidden" value="@{{ lng }}">
                <input name="id" type="hidden" value="@{{ place_id }}">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <div class="card">
                    <div class="container-fluid">
                        <div class="wrapper row">
                            <div class="preview col-md-6">
                                <div class="preview-pic tab-content">
                                    @{{#photoUrls}}
                                        <div class="tab-pane text-center" id="pic-@{{index}}"><img src="@{{url}}"/></div>
                                    @{{/photoUrls}}
                                </div>
                                <ul class="preview-thumbnail nav nav-tabs">
                                    @{{#photoUrls}}
                                        <li><a data-target="#pic-@{{index}}" data-toggle="tab"><img src="@{{url}}"/></a></li>
                                    @{{/photoUrls}}
                                </ul>
                            </div>
                            <div class="details col-md-6">
                                <h3 class="product-title"><a target="_blank" href="@{{ website }}">@{{name}}</a></h3>
                                <div class="rating">
                                    <div class="stars">
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                    <span class="review-no">Ogólna ocena @{{ rating }} Gwiazdki</span>
                                </div>
                                <div class="product-description">
                                    @{{#reviews}}
                                    <p>
                                        @{{ text }}
                                    </p>
                                    @{{/reviews}}
                                </div>
                                <h4 class="tel"><a href="tel:+@{{ international_phone_number }}">@{{ international_phone_number }}</a> <span style='color: @{{ status_color }};'>@{{ status }}</span></h4>
                                <p class="vote">
                                    <strong>@{{ address_components.2.short_name }}</strong>
                                    @{{ address_components.1.short_name }} @{{ address_components.0.short_name }}</p>
                                {{--<h5 class="sizes">sizes:
                                    <span class="size" data-toggle="tooltip" title="small">s</span>
                                    <span class="size" data-toggle="tooltip" title="medium">m</span>
                                    <span class="size" data-toggle="tooltip" title="large">l</span>
                                    <span class="size" data-toggle="tooltip" title="xtra large">xl</span>
                                </h5>--}}
                                {{--<h5 class="colors">colors:
                                    <span class="color orange not-available" data-toggle="tooltip"
                                          title="Not In store"></span>
                                    <span class="color green"></span>
                                    <span class="color blue"></span>
                                </h5>--}}
                                <div class="action">
                                    <button class="show-path btn btn-default btn-lg" type="button">POKAZ TRASE</button>
                                    <button class="street_view btn btn-default btn-info btn-lg" type="button"><i class="fa fa-road" aria-hidden="true"></i></button>
                                    <button class="invite btn btn-default btn-primary btn-lg" type="button"><i class="fa fa-random" aria-hidden="true"></i></button>
                                    <button class="quit btn btn-default btn-danger btn-lg hidden" type="button"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>