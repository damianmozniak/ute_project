<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>UTE Projekt</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {!! Html::style('css/bootstrap/css/bootstrap.css') !!}
    {!! Html::style('css/animate.css') !!}
    {!! Html::script('js/jquery.min.js') !!}
    {!! Html::script('js/bootstrap-notify.min.js') !!}
</head>
<body>
    @if($errors->first('msg'))
        <script>
            var type = "{{ $errors->first('type') }}";
            var title = "{{ $errors->first('title') }}";
            var msg = "{{ $errors->first('msg') }}";

            $.notify({
                icon: 'glyphicon glyphicon-warning-sign',
                title: title,
                message: msg
            },{
                type: type,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });
        </script>
    @endif
    {!! $errors->login->first('email') !!}
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-6 col-sm-12 col-lg-offset-3">
                <form method="POST" action="{{ URL::route('user.do_signup') }}">
                    {!! csrf_field() !!}
                    <h2>Zarejestruj się</h2>
                    <div class="form-group">
                        <label for="email" class="sr-only">Email</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Adres Email" required="required" autofocus="">
                    </div>
                    <div class="form-group">
                        <label for="password" class="sr-only">Hasło</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Hasło" required="required">
                    </div>

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Zarejestruj się</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>

