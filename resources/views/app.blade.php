<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>UTE Projekt</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ Session::token() }}">

    {!! Html::style('css/bootstrap/css/bootstrap.css') !!}
    {!! Html::style('css/app.css') !!}
    {!! Html::style('css/animate.css') !!}
    {!! Html::style('css/rangeslider.css') !!}
    {!! Html::style('css/font-awesome/css/font-awesome.min.css') !!}

    {!! Html::script('js/jquery.min.js') !!}
    {!! Html::script('css/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('js/bootstrap-notify.min.js') !!}

    {!! Html::script('js/main.js') !!}
    {!! Html::script('js/map_style.js') !!}
    {!! Html::script('js/map.js') !!}
    {!! Html::script('js/rangeslider.min.js') !!}
    {!! Html::script('js/mustache.min.js') !!}
    {!! Html::script('js/jquery.touchSwipe.min.js') !!}
    {!! Html::script('js/functions.js') !!}

    @if (Auth::check())
        <script>
            window.userID = "{{ Auth::user()->id }}";
        </script>
        {!! Html::script('js/account_edit.js') !!}
    @endif
    @if(env('APP_DEBUG'))
        {!! Html::script('js/dev.js') !!}
    @endif
</head>
<body>
@if(isset($msg))
    <script>
        $.notify({
            icon: 'glyphicon glyphicon-warning-sign',
            title: "{{ $msg['title'] }}",
            message: "{{ $msg['text'] }}"
        }, {
            type: "{{ $msg['type'] }}",
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
    </script>
@endif

@include('includes/sidebar_left')
@include('includes/sidebar_right')
@include('includes/top_buttons')


@include('includes/modals/categories')
@include('includes/categories_buttons')
@include('includes/modals/place_details')
@include('includes/modals/panorama')

@yield('content')

@if (Auth::check())
    @include('includes/modals/account_edit')
@endif
<button id="follow" type="button" class="btn btn-default" title="Follow">
    <i class="fa fa-location-arrow" aria-hidden="true"></i>
</button>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACMYkoeX68uMXkhF2s7uC7JGssMago_o8&callback=initMap&libraries=places,geometry"
        async defer></script>

@if (session('place_id'))
    <script>
        setTimeout(function(){
            showPlaceDetails('{{ session('place_id') }}');
        },1000);
    </script>
@else
    <script>//TODO: ZMIENIC ZEBY NIE BYLO TUTAJ TIMEOUTA... CZEKAJ AZ ZALADUJE WSZYSTKO (WLACZNIE Z GOOGLE MAPS)
        setTimeout(function(){
            //LOAD LAST
            if (localStorage.getItem("last_show")) {
                showPlaceDetails(localStorage.getItem("last_show"), true);
            }
        },1000);
    </script>
@endif
</body>
</html>
