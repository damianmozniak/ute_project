# Jak uruchomić projekt
* Instalacja composer'a https://getcomposer.org/

* Włącz konsolę w głównym katalogu projetu (tam gdzie znajduje się plik readme.md)

* Wpisz w konsoli 'php artisan serve'

Po tych krokach sewer powinien zostać domyślnie uruchimiony pod localhost:8000

### Baza danych ###
Aby prawidłowo zmigrować baze danych (swieżo stworzoną) należy ustawić poszczególne zmienne w pliku .env:

DB_HOST=serwer.bazy_danych.pl

DB_PORT=3306

DB_DATABASE=nazwa_bazy_danych

DB_USERNAME=nazwa_zytkownika_bazy_danych

DB_PASSWORD=haslo_bazy_danych

A następnie uruchomić proces migracji 'php artisan migrate', który stworzy niezbędne tabele
w naszej bazie danych do działania projektu.

# Laravel PHP Framework

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.