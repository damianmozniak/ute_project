<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventListener extends Model
{
    protected $table = "event_listeners";
}
