<?php
Route::group(['middlewareGroups' => ['web']], function() {

    Route::get('/', 'PagesController@index')->name('home');

    Route::any('login', ['as' => 'user.login', 'uses' => 'UserController@login']);
    Route::any('user/do_login', ['as' => 'user.do_login', 'uses' => 'UserController@do_login']);
    Route::any('signup', ['as' => 'user.signup', 'uses' => 'UserController@signup']);
    Route::any('user/do_signup', ['as' => 'user.do_signup', 'uses' => 'UserController@do_signup']);
    Route::any('event/{eventID}', ['uses' => 'EventsController@event_request']);
    Route::any('place/{placeID}', ['uses' => 'PagesController@show_place']);

    Route::group(['middleware' =>['auth']], function ()
    {
        Route::any('logout', ['as' => 'user.logout' , 'uses' => 'UserController@logout']);
        Route::any('user/update/{userID}', ['uses' => 'UserController@update']);
        Route::post('event/user_in/{eventID}', ['uses' => 'EventsController@user_in_event']);
        Route::post('event/quit/{eventID}', ['uses' => 'EventsController@user_quit']);
        Route::get('event/listeners/{eventID}', ['uses' => 'EventsController@get_event_listeners']);
        Route::post('event/{eventID}/lat/{lat}/lng/{lng}', ['uses' => 'EventsController@set_event_listeners_pos']);
    });
});

