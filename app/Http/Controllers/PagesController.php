<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Psy\Util\Json;

class PagesController extends Controller
{
    public function index() {
        if(!Auth::check()) {
            return View('pages.index');
        } else {
            $user = Auth::user();

            return View('pages.index', compact('user'));
        }
	}

	public function show_place(Request $req, $place_id) {
        $user = Auth::user();

        return redirect('/')->with('place_id', $place_id);
    }
}
