<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventListener;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;

class EventsController extends Controller
{
    public function event_request(Request $request, $placeID) {

        //TODO: WYPISAC UZYTKOWNIKA Z POPRZEDNIEGO WYDARZENIA GDY CHCE DOLACZYC DO NOWEGO
        if(Auth::check()) {
            $user = Auth::user();
            $event = Event::where('place_id', '=', $placeID)->first();
            if($request->ajax()) {

                if ($event == null) {
                    $orange_api_key = 'qr1d7R3Ag3gop06s1bzRuySh7fxukfSA';
                    $event = new Event;
                    $event->place_id = $placeID;
                    $event->save();

                    $event_listener = new EventListener;
                    $event_listener->user_id = $user->id;
                    $event_listener->event_id = $event->id;
                    $event_listener->save();

                    //ZAPRASZANIE GOSCI //WYWALA BLAD.. PRZENOSZE ZADANIE NA JS
                    //$client = new Client();
                    //$res = $client->request('GET', 'https://apitest.orange.pl/Localization/v1/GeoLocation?msisdn=48506869652&apikey=qr1d7R3Ag3gop06s1bzRuySh7fxukfSA');
                    /*$response = $client->get(
                        'https://apitest.orange.pl/Localization/v1/GeoLocation',
                        [
                            'query' => [
                                'msisdn' => '48506869652',
                                'apikey' => 'qr1d7R3Ag3gop06s1bzRuySh7fxukfSA'
                            ]
                        ]
                    )->json();*/

                    /*$res = $client->request('GET', 'https://apitest.orange.pl/Localization/v1/GeoLocation',[
                        'query' => ['msisdn' => '48506869652', 'apikey' => 'qr1d7R3Ag3gop06s1bzRuySh7fxukfSA']
                    ]);*/

                    //return response()->json(['response' => "test"]);
                    $users_to_invite = User::where('id', '!=', $user->id)->where('phone', '!=', 0)->whereRaw('LENGTH(phone) = 9')->pluck('phone')->toArray();

                    return response()->json(
                        ['response' => [
                            'type' => 'success',
                            'msg' => 'Wydarzenie zostało utworzone i zaproszono pobliskie osoby',
                            'users' => $users_to_invite,
                            'user_phone' => $user->phone,
                            'orange_api_key' => $orange_api_key
                        ]
                        ]);
                } else {
                    $event_listener = EventListener::where('user_id', '=', $user->id)->where('event_id', '=', $event->id)->first();

                    if ($event_listener == null) {
                        $event_listener = new EventListener;
                        $event_listener->user_id = $user->id;
                        $event_listener->event_id = $event->id;
                        $event_listener->save();


                        return response()->json(['response' => ['type' => 'success', 'msg' => 'Wydarzenie już istnieje, zostałeś dołączony']]);
                    } else {

                        return response()->json(['response' => ['type' => 'success', 'msg' => 'Wydarzenie już istnieje i uczestniczysz w nim']]);
                    }
                }
            }

        } else {
            return response()->json(['response' => ['type' => 'info', 'msg' => 'Musisz się najpierw zalogować.', 'fail' => true]]);
        }
	}

    public function user_in_event(Request $request, $placeID) {
        if(Auth::check()) {
            $user = Auth::user();
            $event = Event::where('place_id', '=', $placeID)->first();
            if($event != null) {
                $event_listener = EventListener::where('user_id', '=', $user->id)->where('event_id', '=', $event->id)->first();
                if($event_listener == null)
                    return response()->json(false);
                else
                    return response()->json(true);
            }
        }else{
            return response()->json(['response' => 'not logged in']);
        }
    }

    public function user_quit(Request $request, $placeID) {
        if(Auth::check()) {
            $user = Auth::user();
            $event = Event::where('place_id', '=', $placeID)->first();

            $event_listener = EventListener::where('user_id', '=', $user->id)->where('event_id', '=', $event->id)->first();
            $event_listener->delete();

            if(EventListener::where('event_id', '=', $event->id)->first() == null)
                $event->delete();

            return response()->json(['response' => ['type' => 'success', 'msg' => 'Zostałeś wypisany z wydarzenia']]);
        }else{
            return response()->json(['response' => 'not logged in']);
        }
    }

    public function get_event_listeners(Request $request, $event_id) {
        if(Auth::check()) {
            $user = Auth::user();
            $event = Event::where('place_id', '=', $event_id)->first();

            $event_listeners = EventListener::where('event_id', '=', $event->id)->get();

            return response()->json(['response' => ['listeners' => $event_listeners]]);
        }
    }

    public function set_event_listeners_pos(Request $request, $event_id, $lat, $lng) {
        if(Auth::check()) {
            $user = Auth::user();
            $event = Event::where('place_id', '=', $event_id)->first();
            $event_listener = EventListener::where('user_id', '=', $user->id)->where('event_id', '=', $event->id)->first();
            $event_listener->lat = $lat;
            $event_listener->lng = $lng;
            $event_listener->save();
        }
    }
}
