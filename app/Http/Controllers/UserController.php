<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Hash;
use App\User;
use MongoDB\Driver\Exception\Exception;

class UserController extends Controller
{
	
    public function login()
    {
    	// show the form
    	if(Auth::check())
    		return redirect()->route('home');

    	return view('pages.login');
    }
    
    public function signup()
    {
    	return view('pages.register');
    }
    
    public function do_signup(Request $request)
    {
    	$rules = array('email' => 'unique:users|required|email','password' => 'required');
    	$validator = Validator::make(Input::all(), $rules);

    	if($validator->fails())
    	{
            $message = [
                'type' => 'info',
                'title' => 'Nie udało się zarejestrować: ',
                'msg' => 'Niepoprawny email / użytkownik istnieje. Sróbuj ponownie.'
            ];

    		return  redirect()->route('user.signup')
    		->withErrors($message) // send back all errors to the login form
    		->withInput(Input::except('password'));
    	}

    	$email=$request->input('email');
   		$password=$request->input('password');
   		
   		$user=new User;
   		$user->name = "user";
   		$user->email = $email;
   		$user->password = Hash::make($password);
   		$user->save();


        //AUTOLOGIN
        Auth::attempt([
            'email' => $email,
            'password'  => $password
        ]);

    	return Redirect::route('home');
    }
    
	public function do_login()
	{	
		$rules = array('email' => 'required|email','password' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()) {
            $message = [
                'type' => 'info',
                'title' => 'Brak autoryzacji: ',
                'msg' => 'Niepoprawny email. Sróbuj ponownie.'
            ];

			return  redirect()->route('user.login')
			->withErrors($message,'login') // send back all errors to the login form
			->withInput(Input::except('password'));

		}else{
		
		    // create our user data for the authentication
			$userdata = array(
								'email' => Input::get('email'),
								'password'  => Input::get('password')
				  			  );
			// attempt to do the login
			if (Auth::attempt($userdata)) 
			{
				return redirect()->intended('/');
			} 
			else 
			{
				$message = [
                    'type' => 'info',
                    'title' => 'Brak autoryzacji: ',
                    'msg' => 'Niepoprawny login lub hasło. Sróbuj ponownie.'
                ];
				return redirect()->route('user.login')->withErrors($message,'login');
			}
		}
	}
	
	public function logout()
	{
		Auth::logout(); // log the user out of our application
		return redirect()->route('home');// redirect the user to the login screen
	}

	public function update(Request $req, $userID) {

        $user = Auth::user();

        if($user->id == $userID) {
            $data = $req['data'];

            $user->name = $data['username'];
            $user->phone = $data['phone'];
            $user->save();

            return response()->json(['response' => "success"]);
        } else {
            return response()->json(['response' => 'not authorized'], 401);
        }
    }
}
