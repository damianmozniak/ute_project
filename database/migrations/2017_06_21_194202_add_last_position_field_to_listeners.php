<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastPositionFieldToListeners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_listeners', function(Blueprint $table)
        {
            $table->double('lat', 15, 8);
            $table->double('lng', 15, 8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_listeners', function(Blueprint $table)
        {
            $table->dropColumn('lat');
            $table->dropColumn('lng');
        });
    }
}
